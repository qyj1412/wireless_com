import machine
import network
import time
import socket
import _thread
import binascii

# 初始化wifi
wifi_name = 'qyjnb'#'Xinje AP'
wifi_passwd = '1234567890'#'xinje85134136'

led = machine.Pin(2, machine.Pin.OUT)
led.value(0)

wlan = network.WLAN(network.STA_IF)
wlan.active(False)
time.sleep_ms(1000)
wlan.active(True)

if not wlan.isconnected():
    wlan.connect(wifi_name, wifi_passwd)

while not wlan.isconnected():
    print('wait connetting...', wlan.status())
    led.value(1)
    time.sleep_ms(300)
    led.value(0)
    time.sleep_ms(300)


print('ip', wlan.ifconfig())
led.value(1)


def uart_recv_event():
    global send_sem
    send_sem.release()


uart = machine.UART(2, 115200, timeout=10)

key = machine.Pin(0, machine.Pin.IN, machine.Pin.PULL_UP)
# key.irq(trigger=machine.Pin.IRQ_FALLING, handler=lambda: None)

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server.bind((wlan.ifconfig()[0], 5677))
client_addr = ('192.168.118.197', 7777)

def udp_send():
    global server, client_addr, send_sem, uart

    while 1:
        datagram = uart.read()
        if datagram is not None:
            print('prepare send')
            for i in datagram[:10]:
                print(i, chr(i))
            server.sendto(datagram[:1023], client_addr)


def udp_recv():
    global server, uart, client_addr

    while 1:
        rdata, rclient = server.recvfrom(1024)
        client_addr = rclient
        print('recv', len(rdata), rdata)
        uart.write(rdata)


_thread.start_new_thread(udp_send, ())
_thread.start_new_thread(udp_recv, ())


while 1:
    time.sleep_ms(1000)

