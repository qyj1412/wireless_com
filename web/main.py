from nicegui import ui
import time
import socket
from threading import Thread, Lock
from MyQR import myqr



def udp_recv():
    global client, uart_log

    while 1:
        rdata, server = client.recvfrom(10240)
        rdata_ascii = ''
        for i in rdata:
            rdata_ascii = rdata_ascii + chr(i)
        print(rdata_ascii)
        # uart_log.push(f"{time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())}|From {server}> {ascii(rdata)}")



def udp_send():
    global server, client, send_input

    while 1:
        send_sem.acquire()
        sdata = send_input.value + '\n'
        print(send_input.value)
        for i in sdata:
            print('sdata', i)
        sdata_byte = bytearray()
        for i in sdata:
            sdata_byte.append(ord(i))
        print('send', sdata_byte, len(sdata_byte), len(sdata))
        client.sendto(sdata_byte, server)
        send_input.set_value('')
        


myqr.run(words=f"http://{socket.gethostbyname(socket.getfqdn(socket.gethostname()))}:8080",version=9,
    save_name="visit.png",
    save_dir=".")



ui.label('扫描访问无线串口Web端')
ui.image(r'./visit.png').classes('w-80 h-80 bg-blue-100')


ui.label('无线串口数据')
uart_log = ui.log(max_lines=100).classes('w-full h-200')

send_sem = Lock()
send_sem.acquire(timeout=0)
send_input = ui.input(label='Tx').on('keydown.enter', lambda: send_sem.release())

ui.label('Key触发日志')
key_log = ui.log(max_lines=10).classes('w-full h-200')


ui.label('LED灯状态')
led = ui.switch('LED', value=False)
ui.button('刷新', on_click=lambda: ui.notify('刷新'))

ui.run()


# server = ('192.168.1.11', 5677)
server = ('192.168.118.135', 5677)
client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client.bind((socket.gethostbyname(socket.getfqdn(socket.gethostname())), 7777))
# client.bind((socket.gethostbyname(socket.getfqdn(socket.gethostname())), 7777))


send_thread = Thread(target=udp_send)
recv_thread = Thread(target=udp_recv)
recv_thread.start()
send_thread.start()